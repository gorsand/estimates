#!/usr/bin/env python3

import sys
import os
import re
from unidecode import unidecode

DATA_DIR = "current_project"
PRICES_FNAME = "stocks_weekly.csv"
ESTIMATES_FMASK = "estimates_sales_{}.csv"
OUTPUT_DIR = "data_py"
START_YEAR = 2013
END_YEAR = 2020
MIN_DATA_POINTS = 50

regex = re.compile("[^0-9a-zA-Z_-]")

# prepare output dirs
if not os.path.exists(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)

estim_dir = os.path.join(OUTPUT_DIR, "estims")
if not os.path.exists(estim_dir):
    os.mkdir(estim_dir)

price_dir = os.path.join(OUTPUT_DIR, "prices")
if not os.path.exists(price_dir):
    os.mkdir(price_dir)

def brief_ticker(s):
    # make ticker be appropriate as a file name
    return regex.sub('_', unidecode(str(s.encode(), encoding="utf-8"))).replace("_US_Equity", "").strip("_")

def skip_ticker(s):
    return s == "" or s.startswith("2017Y") or s.startswith("Unnamed") or s.endswith(".1")

def parse_date(s):
    if s == '':
        return None
    # check format DD.MM.YYYY
    a = s.split('.')
    if len(a) != 3:
        return None
    if len(a[2]) != 4 or len(a[1]) != 2 or len(a[0]) != 2:
        return None
    try:
        # check year, month, day are integers
        _, _, _ = [int(x) for x in a]
        return a[2] + "-" + a[1] + "-" + a[0]
    except:
        return None

def parse_col(s):
    s = unidecode(str(s.encode(), encoding="utf-8")).replace(" ", "")
    try:
        return float(s)
    except:
        return None

def load_data(errs, fname, sep, skip_before_hdr, skip_after_hdr, start_dt, end_dt, only_tickers=None):
    data = {}
    skip_tk = lambda tk: skip_ticker(tk) or (only_tickers and tk not in only_tickers)
    with open(fname) as f:
        # skip garbage before hdr
        for _ in range(skip_before_hdr):
            next(f)
        # parse header
        s = next(f)
        tickers = [brief_ticker(t) for t in s.split(sep)[1:]]
        if len(tickers) < 1:
            exit("File '{}': no tickers in line 5".format(fname))
        # skip garbage after hdr
        for _ in range(skip_after_hdr):
            next(f)
        # read data rows
        tot_lines = 0
        good_lines = 0
        ncols = len(tickers) + 1
        for n, line in enumerate(f):
            cols = line.strip().split(sep)
            if len(cols) < 1:
                # skip empty lines (if any)
                continue
            # parse date (NOTE: represented as string, not datetime object)
            dt = parse_date(cols[0])
            if dt is None:
                errs.write("File '{}', line {}: expecetd DD.MM.YYYY, got '{}' as first col\n".format(fname, n, cols[0]))
                continue
            if dt < start_dt or dt > end_dt:
                # take only needed dates (NOTE: string comparison, YYYY-MM-DD format matters)
                continue
            tot_lines += 1
            if len(cols) != ncols:
                # don't warn on extra columns, only on missing ones
                if len(cols) < ncols:
                    errs.write("File '{}', Line {}: have {} cols, required {}\n".format(fname, n, len(cols), ncols))
                continue
            good_lines += 1
            # process columns
            for k, col in enumerate(cols[1:]):
                tk = tickers[k]
                if skip_tk(tk):
                    continue
                # parse value
                val = parse_col(col)
                if val is None:
                    if s != "" and not s.startswith("#"):
                        errs.write("File {}, line {}: non-number '{}' in col {}\n".format(fname, n, col, k))
                        continue
                # save data
                if tk not in data:
                    data[tk] = {}
                data[tk][dt] = val
        print("  lines total: {}".format(tot_lines))
        print("  lines skipped: {}".format(tot_lines - good_lines))
        print("  tickers total: {}".format(len(tickers)))
        print("  tickers with data: {}".format(len(data)))
        return [data, [tk for tk in tickers if not skip_tk(tk)]]

def load_all_estims(errs):
    all_data = {}
    all_tickers = {}
    print("Loading estimates ...")
    for year in range(START_YEAR, END_YEAR+1):
        start_dt = str(year) + "-01-01"
        end_dt = str(year) + "-12-31"
        fname = os.path.join(DATA_DIR, ESTIMATES_FMASK.format(year))
        if not os.path.isfile(fname):
            continue
        print("Processing '{}' ...".format(fname))
        data, tickers = load_data(errs, fname, ",", 4, 2, start_dt, end_dt)
        # join tickers (with data or not)
        for tk in tickers:
            all_tickers[tk] = True
        # join data
        for tk, tkd in data.items():
            if tk not in all_data:
                all_data[tk] = {}
            for dt, val in tkd.items():
                all_data[tk][dt] = val
    print("Tickers total: {}".format(len(all_tickers)))
    print("Tickers skipped: {}".format([tk for tk in all_tickers if tk not in all_data]))
    return all_data

def load_prices(errs, tickers):
    print("Loading prices ...")
    start_dt = str(START_YEAR) + "-01-01"
    end_dt = str(END_YEAR) + "-12-31"
    fname = os.path.join(DATA_DIR, PRICES_FNAME)
    if not os.path.isfile(fname):
        exit("Prices file '{}' not found".format(fname))
    data, _ = load_data(errs, fname, ";", 3, 2, start_dt, end_dt, tickers)
    print("Tickers with prices: {}".format(len(data)))
    return data

def save_to_files(data, outdir):
    with open(os.path.join(outdir, "tickers.lst"), "w") as tk_file:
        for tk in sorted(data.keys()):
            tkd = data[tk]
            if len(tkd) < MIN_DATA_POINTS:
                continue
            tk_file.write("{}\n".format(tk))
            fname = os.path.join(outdir, "{}.txt".format(tk))
            with open(fname, "w") as data_file:
                for dt in sorted(tkd.keys()):
                    val = tkd[dt]
                    if int(val) == val:
                        val = int(val)
                    data_file.write("{} {}\n".format(dt, val))

# process data
with open(os.path.join(OUTPUT_DIR, "errors.txt"), "w") as errs:
    estim_data = load_all_estims(errs)
    price_data = load_prices(errs, estim_data)
    print("Saving estims ...")
    save_to_files(estim_data, estim_dir)
    print("Saving prices ...")
    save_to_files(price_data, price_dir)
